# MediaMonks BA - Templating Frontend test

In the attached [zip file](https://drive.google.com/a/mediamonks.com/file/d/0B1Y9RcrhhkVNeHVBRGUteEtPVEU/view?usp=sharing), you can find an mp4 file showing our old MediaMonks flash website on how to be a Monk. 

The test is to rebuild this website in HTML, CSS and JavaScript:  
 * You are NOT allowed to use an external framework or library (i.e. jQuery or Bootstrap)
 * Aim to replicate the design and functionality as close as possible to the flash site
 * Write clean, scalable and reusable code
 * You are allowed to use CSS preprocessors and ES6/ES7
 * Extra points if it also works on mobile
 * Make it as awesome as possible

The assets that you need are included as well in the zip file (please don’t send a .rar file).

This test should be done in approximately 4 to 8 hours. When you are finished, please send us your result either in a zipped file via email or shared repo. Make sure the test is properly built so that it will run directly in the browser with no additional steps. If you chose to share a repo, make sure you provide access to:

 * thijs@mediamonks.com
 * martin.termini@mediamonks.com
 * ronald.mendez@mediamonks.com
 * fabricio.mouzo@mediamonks.com
 * hendrik-jan@mediamonks.com

# Website
Online: [kleith.gitlab.io/mediamonks-frontend](https://kleith.gitlab.io/mediamonks-frontend/)