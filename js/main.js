(function() {
  'use strict'

  var actualPage = 'first';
  var backgroundPositionsPercent = [0, 13, 22, 37, 51, 65, 80, 101, 101, 115];
  // var backgroundPositions = [0, -1170, -2050, -3350, -4630, -5920, -7260, -9110, -9110, -10470];

  var lis = document.querySelectorAll('.pages ul li');

  // click event navigation page
  lis.forEach(function(li) {
    li.addEventListener('click', function() {
      updatedPage(this.textContent);
    });
  });

  // click event arrow left
  document.querySelector('.arrows.arrow-left').addEventListener('click', function() {
    switch (actualPage) {
      case 'last':
        actualPage = '8';
        break;
      case '1':
        actualPage = 'first';
        break;
      default:
        actualPage = (parseInt(actualPage) - 1).toString();
        break;
    }
    updatedPage(actualPage);
  });

  // click event arrow right
  document.querySelector('.arrows.arrow-right').addEventListener('click', function() {
    switch (actualPage) {
      case 'first':
        actualPage = '1';
        break;
      case '8':
        actualPage = 'last';
        break;
      default:
        actualPage = (parseInt(actualPage) + 1).toString();
        break;
    }
    updatedPage(actualPage);
  });

  function updatedPage(page) {
    actualPage = page;
    var tabs = document.querySelector('.tabs');
    var lis = document.querySelectorAll('.pages ul li');
    var textPage = document.querySelector('.pages .text-page');
    var arrorLeft = document.querySelector('.arrows.arrow-left');
    var arrorRight = document.querySelector('.arrows.arrow-right')

    tabs.querySelector('[class*=tab-].active').classList.remove('active');
    tabs.querySelector(`.tab-${actualPage}`).classList.add('active');

    document.querySelector('.pages ul li.active').classList.remove('active');
    textPage.querySelector('.actual-page').textContent = actualPage;
    
    switch (actualPage) {
      case 'first':
        textPage.classList.remove('is-visible');
        lis[0].classList.add('active');
        arrorLeft.classList.remove('is-visible');
        arrorRight.classList.add('is-visible');
        setPosition(backgroundPositionsPercent[0]);
        break;
      case 'last':
        textPage.classList.remove('is-visible');
        lis[lis.length - 1].classList.add('active');
        arrorRight.classList.remove('is-visible');
        arrorLeft.classList.add('is-visible');
        setPosition(backgroundPositionsPercent[lis.length - 1]);
        break;
      default:
        arrorLeft.classList.add('is-visible');
        arrorRight.classList.add('is-visible');
        textPage.classList.add('is-visible');
        lis[actualPage].classList.add('active');
        setPosition(backgroundPositionsPercent[parseInt(actualPage)]);
        break;
    }
  }

  function setPosition(position) {
    document.querySelector('.main').style.backgroundPositionX = `${position}%`;
  }
})()